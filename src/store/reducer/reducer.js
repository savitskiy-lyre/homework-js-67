const initState = {
    btnValues: [
        1, 2, 3, '*',
        4, 5, 6, '/',
        7, 8, 9, '-',
        '<', 0, 'E', '+',
        '='
    ],
    displayValue: '0',
};

const reducer = (state = initState, action) => {
    if (action.type === 'ADD_VALUE') {
        if (state.displayValue === '0') return {...state, displayValue: action.payload};
        const currentValue = parseInt(action.payload) || action.payload === '0' ? action.payload : ` ${action.payload} `;
        return {...state, displayValue: state.displayValue + currentValue}
    }
    if (action.type === 'REMOVE_ITEM') {
        if (!parseInt(state.displayValue[state.displayValue.length - 1]) && state.displayValue !== '0') {
            return {...state, displayValue: state.displayValue.slice(0, -3)}
        }
        return {...state, displayValue: state.displayValue.slice(0, -1)}
    }
    if (action.type === 'RESET') {
        return {...state, displayValue: initState.displayValue}
    }
    if (action.type === 'EQUALITY') {
        let total;
        try {
            total = eval(state.displayValue);
        } catch {
            total = '0';
        }
        return {...state, displayValue: total};
    }

    return state;
}

export default reducer;