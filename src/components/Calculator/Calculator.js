import React, {useCallback, useMemo} from 'react';
import './Calculator.css';
import {useDispatch, useSelector} from "react-redux";
import CalculatorButton from "./CalculatorButton/CalculatorButton";
import Display from "./Display/Display";

const Calculator = () => {
    const dispatch = useDispatch();
    const btnValues = useSelector(store => store.btnValues);
    const onAddValue = useCallback(e => {
        return dispatch({type: 'ADD_VALUE', payload: e.target.innerText})
    }, [])
    const onRemoveItem = useCallback(() => dispatch({type: 'REMOVE_ITEM'}), []);
    const onReset = useCallback(() => dispatch({type: 'RESET'}), []);
    const onEquality = useCallback(() => dispatch({type: 'EQUALITY'}), []);
    const btnsEL = useMemo(() => {
        const btns = [];
        for (let i = 0; i < 3; i++) {
            const rowEl = [];
            for (let j = 0; j < 4; j++) {
                rowEl.push(
                    <CalculatorButton
                        value={btnValues[j + (i * 4)]}
                        onClickHandler={onAddValue}
                        key={Math.random()}
                    />)
            }
            btns.push(<div key={Math.random()}>{rowEl}</div>)
        }
        btns.push(
            <div key={Math.random()}>
                <CalculatorButton value={btnValues[btnValues.length - 5]}
                                  onClickHandler={onRemoveItem}/>
                <CalculatorButton value={btnValues[btnValues.length - 4]}
                                  onClickHandler={onAddValue}/>
                <CalculatorButton value={btnValues[btnValues.length - 3]}
                                  onClickHandler={onReset}/>
                <CalculatorButton value={btnValues[btnValues.length - 2]}
                                  onClickHandler={onAddValue}/>
            </div>)
        return btns;
    }, [])
    return (
        <div className='calculator-wrapper'>
            <Display/>
            <div className="btns-block">
                {btnsEL}
                <CalculatorButton value={btnValues[btnValues.length - 1]} onClickHandler={onEquality}/>
            </div>
        </div>
    );
};

export default Calculator;