import React from 'react';
import './Display.css';
import {useSelector} from "react-redux";

const Display = () => {
    const displayValue = useSelector(store => store.displayValue);
    return (
        <div className="display-wrapper">
            <div className="display-item">{displayValue}</div>
        </div>
    );
};

export default Display;