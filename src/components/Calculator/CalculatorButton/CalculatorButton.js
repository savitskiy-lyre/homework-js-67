import React from 'react';
import './CalculatorButton.css';

const CalculatorButton = ({value, onClickHandler}) => {
    const btnClass = value !== '=' ? "btn-wrapper" : "btn-wrapper btn-equality";
    return (
        <button className={btnClass} onClick={onClickHandler}>
            {value}
        </button>
    );
};

export default CalculatorButton;